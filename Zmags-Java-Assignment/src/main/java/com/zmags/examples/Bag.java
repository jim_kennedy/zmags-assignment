package com.zmags.examples;

import java.util.AbstractCollection;
import java.util.Iterator;

/**
* <h1>Custom Collection type similar to an ArrayList</h1> 
* The collection can store multiple instances of equal elements and does not preserve the order 
* of its elements.
*
* @author  Jim Kennedy
* @version 1.0
* @since   2017-10-03
*/

public class Bag<E> extends AbstractCollection<E> {

	@SuppressWarnings("unchecked")
	private E data[] = (E[]) new Object[1024];
	private int size = 0;

	/**
	 * Removes the first occurrence of the specified element from this Bag, if it is
	 * present. If the Bag does not contain the element, it is unchanged.
	 * 
	 * @param element an element to be removed from this list, if present
	 * @return <tt>true</tt> if list contained the specified element
	 */
	public boolean remove(Object element) {
		for (int i = 0; i < size; i++) {
			if (element == data[i]) {
				data[i] = data[--size]; // overwrite item to remove with last element
				data[size] = null; // null last element, so gc can do its work
				return true;
			}
		}

		return false;
	}

	/**
	 * Returns the number of occurrences of the specified element.
	 * 
	 * @param element the element to be removed from this list, if present
	 * @return <tt>true</tt> if list contained the specified element
	 */
	public int occurrences(Object element) {
		int counter = 0;
		for (int i = 0; i < size; i++) {
			if (element == data[i]) {
				counter++;
			}
		}

		return counter;
	}

	/**
	 * Returns the element at the specified position in this bag.
	 *
	 * @param index  the index of the element to return
	 * @return element the element at the specified position in this bag
	 */
	public E get(int index) {
		return data[index];
	}

	/**
	 * Returns the number of elements in this bag.
	 *
	 * @return the number of elements in this bag
	 */
	public int size() {
		return size;
	}

	/**
	 * Returns true if this bag contains no elements.
	 *
	 * @return <tt>true</tt> if this bag contains no elements
	 */
	public boolean isEmpty() {
		return size == 0;
	}

	/**
	 * Removes all of the elements from this bag. The bag will be empty after this
	 * call returns.
	 */
	public void clear() {
		// null all elements so gc can clean up
		for (int i = 0; i < size; i++) {
			data[i] = null;
		}

		size = 0;
	}

	/**
	 * Adds the specified element to the end of this bag. If needed also increases
	 * the capacity of the bag.
	 *
	 * @return <tt>true</tt> if list contained the specified element
	 */
	public boolean add(E element) {
		// if size greater than data capacity increase capacity
		if (size == data.length) {
			grow();
		}

		data[size++] = element;
		return true;
	}

	/**
	 * Expand the size of the collection to make room for new elements.
	 */
	@SuppressWarnings("unchecked")
	private void grow() {
		Object[] oldData = data;
		int newCapacity = (oldData.length * 3) / 2 + 1;
		data = (E[]) new Object[newCapacity];
		System.arraycopy(oldData, 0, data, 0, oldData.length);
	}

	/**
	 * Returns an iterator for this bag.
	 *
	 * @return true if this bag contains no elements
	 */
	public Iterator<E> iterator() {
		return new BagIterator();
	}

	/**
	 * Iterator class for this bag
	 */
	private class BagIterator implements Iterator<E> {
		int i = 0;

		public boolean hasNext() {
			return i < size;
		}

		public E next() {
			return data[i++];
		}
	}
}