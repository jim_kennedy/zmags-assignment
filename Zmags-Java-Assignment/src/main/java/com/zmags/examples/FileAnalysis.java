package com.zmags.examples;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Pattern;

/**
* <h1>Returns a list of the top ten occurring words from the input text file.</h1>
* The FileAnalysis program reads from the input file splitting words based on 
* non-alphabetic character delimiters. Each word is added to the return list 
* and the word counter incremented when an occurrence is found.
*
* @author  Jim Kennedy
* @version 1.0
* @since   2017-10-03
*/
public class FileAnalysis {

	/**
    * Accepts a text filename as an input parameter parsing the input file
    * based on a non-alphabetic character pattern building a word list of the top ten 
    * occurring words. The word list is sorted in descending order by the number of 
    * word occurrences
    * 
    * @param fileName This is the input filename
    * @return List returns the list of top ten recurring words
    */
	public List<Entry<String, Integer>> topTenWordList(String fileName) {

		Map<String, Integer> wordMap = new HashMap<String, Integer>();
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource(fileName).getFile());

		try (FileInputStream fileInputStream = new FileInputStream(file);
				DataInputStream dataInputStream = new DataInputStream(fileInputStream);
				BufferedReader bufferedReader = new BufferedReader(
						new InputStreamReader(dataInputStream, StandardCharsets.UTF_8))) {

			// \P{L} matches a single character belonging to the "letter" category 
			Pattern pattern = Pattern.compile("[\\P{L}]+");
			String line = null;
			while ((line = bufferedReader.readLine()) != null) {
				line = line.toLowerCase();
				String[] words = pattern.split(line);
				for (String word : words) {
					if (wordMap.containsKey(word)) {
						wordMap.put(word, (wordMap.get(word) + 1));
					} else {
						wordMap.put(word, 1);
					}
				}
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}

		List<Entry<String, Integer>> topTenlist = sortByDecreasing(wordMap);
		if (topTenlist.size() >= 10) {
			return topTenlist.subList(0, 9);
		} else {
			return topTenlist;
		}

	}

	/**
    * Sorts the input Map returning a List of elements in descending order.
    * 
    * @param wordMap a Map of <String, Integer> elements
    * @return List returns a List in descending order
    */
	private List<Entry<String, Integer>> sortByDecreasing(Map<String, Integer> wordMap) {
		Set<Entry<String, Integer>> entries = wordMap.entrySet();
		List<Entry<String, Integer>> list = new ArrayList<Entry<String, Integer>>(entries);
		Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
			public int compare(Map.Entry<String, Integer> entry1, Map.Entry<String, Integer> entry2) {
				return (entry2.getValue()).compareTo(entry1.getValue());
			}
		});
		return list;
	}
}
