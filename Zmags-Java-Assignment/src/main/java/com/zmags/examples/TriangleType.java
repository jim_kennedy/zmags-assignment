package com.zmags.examples;

/**
* <h1>Determine type of Triangle</h1>
* The TriangleType program implements a function that
* that takes three lengths as input arguments and
* determines the type of triangle the sides would create.
*
* @author  Jim Kennedy
* @version 1.0
* @since   2017-10-03
*/
public enum  TriangleType {
	ISOSCELES,
	EQUILATERAL,
	SCALENE,
	ERROR;
	
	/**
    * This method compares the lengths of each triangle side to determine the
    * the type of the resulting triangle
    * 
    * @param side1 first triangle side
    * @param side2 second triangle side
    * @param side3 third triangle side
    * @return String returns the type of triangle
    */
	public static TriangleType getType(int side1, int side2, int side3)
    {
        if(side1<=0||side2<=0||side3<=0)
            throw new IllegalArgumentException("Length of sides cannot be equal to or less than zero");

        if(side1==side2&&side2==side3&&side3==side1)
            return EQUILATERAL;
        else if((side1==side2)||(side2==side3)||(side3==side1))
            return ISOSCELES;
        else if(side1!=side2&&side2!=side3&&side3!=side1)
            return SCALENE;
        else
            return ERROR;
    }
}
