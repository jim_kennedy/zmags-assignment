package com.zmags.examples;

import static org.junit.Assert.*;
import java.util.Iterator;
import org.junit.Before;
import org.junit.Test;
import com.zmags.examples.Bag;

/**
* Test case for {@link com.zmags.examples.Bag}
*
* @author  Jim Kennedy
* @version 1.0
* @since   2017-10-03
 */
public class BagTest {
	
	private Bag<String> bag = null;
	
	/**
	 * Before test method used to create new bag before each test
	 */
	@Before
    public void runOnceBeforeEachTest() {
		bag = new Bag<String>();
		bag.add("One");
		bag.add("Two");
		bag.add("Three");
    }
	
	/**
	 * Test method for {@link com.zmags.examples.Bag#add(java.lang.Object)}.
	 */
	@Test
	public void testAdd() {
		assertTrue("A new entry is added to the Bag",bag.add("One"));
	}

	/**
	 * Test method for {@link com.zmags.examples.Bag#size()}.
	 */
	@Test
	public void testSize() {
		assertTrue("The number of objects in the bag is 3",bag.size()==3);
	}

	/**
	 * Test method for {@link com.zmags.examples.Bag#isEmpty()}.
	 */
	@Test
	public void testIsEmpty() {
		assertFalse("The Bag is not empty",bag.isEmpty());
		bag.clear();
		assertTrue("The Bag is empty",bag.isEmpty());
	}

	/**
	 * Test method for {@link com.zmags.examples.Bag#remove(java.lang.Object)}.
	 */
	@Test
	public void testRemoveObject() {
		assertTrue("Succesfully removed String One from Bag",bag.remove("One"));
	}

	/**
	 * Test method for {@link com.zmags.examples.Bag#occurrences(java.lang.Object)}.
	 */
	@Test
	public void testOccurrences() {
		bag.add("One");
		assertTrue("Number of occurances of the String One",bag.occurrences("One")==2);
	}

	/**
	 * Test method for {@link com.zmags.examples.Bag#iterator()}.
	 */
	@Test
	public void testIterator() {
		Iterator<String> itr = bag.iterator();
		int counter = 0;
	      
		while(itr.hasNext()) {
			String entry = itr.next();
			assertEquals("Bag entry index " + counter + " equals " + entry,bag.get(counter),entry);
			counter++;
		}
	}
}
