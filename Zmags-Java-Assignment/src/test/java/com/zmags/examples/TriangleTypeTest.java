package com.zmags.examples;

import static org.junit.Assert.*;

import org.junit.Test;

/**
* Test case for {@link com.zmags.examples.TriangleType}
*
* @author  Jim Kennedy
* @version 1.0
* @since   2017-10-03
 */
public class TriangleTypeTest {

	/**
	 * Test method for {@link com.zmags.examples.TriangleType#getType(int,int,int)}.
	 */
	@Test
	public void testGetType() {
		// assert statements
		assertEquals("This should be a Scalene triangle",TriangleType.SCALENE,TriangleType.getType(1, 2, 3));
		assertEquals("This should be an Isosceles triangle",TriangleType.ISOSCELES,TriangleType.getType(1, 1, 2));
		assertEquals("This should be an Equilateral triangle",TriangleType.EQUILATERAL,TriangleType.getType(1, 1, 1));
	}
	
	/**
	 * Test method for IllegalArgumentException.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testIllegalArgumentException() {
		TriangleType.getType(0, 1, 2);
	}
}
