package com.zmags.examples;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Map.Entry;
import org.junit.Test;

/**
* Test case for {@link com.zmags.examples.FileAnalysis}
*
* @author  Jim Kennedy
* @version 1.0
* @since   2017-10-03
 */
public class FileAnalysisTest {

	/**
	 * Test method for english text file.
	 */
	@Test
	public void testEnglishFile() {
		FileAnalysis fileAnalysis = new FileAnalysis();
		
		List<Entry<String, Integer>> englishWordList = fileAnalysis.topTenWordList("file/english-words.txt");
		// assert statements
		assertTrue("The Map size should be less then or equal to 10",englishWordList.size()<=10);
		assertTrue("The number of occurances for the first entry in the list should be 167",englishWordList.get(0).getValue()==167);
	}
	
	/**
	 * Test method for greek text file.
	 */
	@Test
	public void testGreekFile() {
		FileAnalysis fileAnalysis = new FileAnalysis();
		
		List<Entry<String, Integer>> greekWordList = fileAnalysis.topTenWordList("file/greek-words.txt");
		// assert statements
		assertTrue("The Map size should be less then or equal to 10",greekWordList.size()<=10);
		assertTrue("The number of occurances for the first entry in the list should be 17",greekWordList.get(0).getValue()==17);

	}

}
