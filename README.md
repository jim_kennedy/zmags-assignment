# ZMAGS Java Coding Assignment #

This project contains the solutions for the three ZMAGS Java Coding Assignment

### What is this repository for? ###

* This respository was created to facilitate the submission of the coding assignment
* Version 1.0

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Assignments ###

* Task 1 Triangles - Write a function to determine the type of a triangle.
* Task 2 Text file analysis - Create functionality that can read an English text file and list the top 10 words, sorted by occurrence in descending order.	
* Task 3 Bag - Implement a generic "bag" container that follows guidelines from the Java Collections library with the following restrictions/requirements:
- It must be possible to store several equal elements in the bag (different from a set).
- It is not required to store each object separately. You can consider equal objects identical.
- Implementation must correctly support generics.
- It is not necessary to add the whole range of operations from the Collection interface.